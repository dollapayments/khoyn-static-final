# Usage

Pass this template to middleman using the following command:

```sh
middleman init $PROJECT_NAME -T hrkfdn/middleman-haml-template
```
